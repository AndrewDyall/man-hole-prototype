﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerKill : MonoBehaviour {

    private GameObject playerDeath;

    public GameObject playerBody;
    public GameObject torch;

    public AudioClip deathSound;
    private AudioSource deathSource;
    // Use this for initialization

    void Awake()
    {
        GameObject sourceObj = new GameObject();
        sourceObj.transform.parent = this.transform;
        deathSource = sourceObj.AddComponent<AudioSource>();
        deathSource.loop = false;
        deathSource.playOnAwake = false;
        if (deathSound != null)
            deathSource.clip = deathSound;
    }

    void Start () {
        playerDeath = GameObject.FindWithTag("Killbox");
    }

    private void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "Killbox")
        {
            Destroy(torch);
            Destroy(playerBody);
            if (deathSound != null)
                deathSource.Play();
            SceneManager.LoadScene("Death_Screen");
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
