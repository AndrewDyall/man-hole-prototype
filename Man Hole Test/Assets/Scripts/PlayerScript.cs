﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {
    public float JumpForce = 200;
    public float MoveSpeed = 2;
    private bool canjump = false;
    public AudioClip jumpSound;
    private AudioSource jumpSource;

    void Awake()
    {
        GameObject sourceObj = new GameObject();
        sourceObj.transform.parent = this.transform;
        jumpSource = sourceObj.AddComponent<AudioSource>();
        jumpSource.loop = false;
        jumpSource.playOnAwake = false;
        if (jumpSound != null)
            jumpSource.clip = jumpSound;
        // Use this for initialization
    }
	// Update is called once per frame
	void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += transform.TransformDirection(Vector3.left) * Time.deltaTime * MoveSpeed;
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position += transform.TransformDirection(Vector3.right) * Time.deltaTime * MoveSpeed;
        }

        if (Input.GetKeyDown(KeyCode.W) && canjump == true)
        {
            //Debug.Log("Jump");
            Jump();

                if(jumpSound != null)
					jumpSource.Play();
        }
    }

    void Jump()
    {
        GetComponent<Rigidbody>().AddForce(Vector3.up * JumpForce);
   }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Platform")
        {
            //Debug.Log("Can Jump");
            canjump = true;
        }

    }

    void OnCollisionExit(Collision col)
    {
        //Debug.Log("No Jump");
        if (col.gameObject.tag == "Platform")
        {
            //Debug.Log("Cant Jump");
            canjump = false;
        }
    }
}
