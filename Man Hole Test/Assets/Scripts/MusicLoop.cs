﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicLoop : MonoBehaviour
{
    public AudioClip introSound;
    private AudioSource introSource;
    public AudioClip loopSound;
    private AudioSource loopSource;

    void Awake()
    {
        GameObject sourceObj = new GameObject();
        sourceObj.transform.parent = this.transform;
        introSource = sourceObj.AddComponent<AudioSource>();
        introSource.loop = false;
        introSource.playOnAwake = true;
        if (introSound != null)
            introSource.clip = introSound;

        GameObject sourceObj2 = new GameObject();
        sourceObj.transform.parent = this.transform;
        loopSource = sourceObj.AddComponent<AudioSource>();
        loopSource.loop = true;
        loopSource.playOnAwake = false;
        if (loopSound != null)
            loopSource.clip = loopSound;

    }


}