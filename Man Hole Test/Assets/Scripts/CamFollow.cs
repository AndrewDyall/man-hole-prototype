﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour {

    public GameObject Player;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(Player != null)
        {
var pos = transform.position;
        pos.y = Player.transform.position.y + 1.5f;
        transform.position = pos;
        }
        
	}
}
