﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Timers;

public class GameplayController : MonoBehaviour 
{
    // Pause Screen
    private bool pause = true;
    public Canvas pauseScreen;

    private Timer timer;
    public Canvas countdownScreen;
    public Text CountdownText;
    //public Button ResumeButton;
    private float countdown = 3;
    private bool finished = false;

    void Awake()
	{
        pauseScreen.enabled = false;

        Time.timeScale = 0f;
        timer = new Timer();
        timer.Elapsed += new ElapsedEventHandler(Countdown);
        timer.Interval = 1000;
        timer.Enabled = true;
        timer.Start();
    }

    private void Countdown(object source, ElapsedEventArgs e)
    {
        countdown--;
        if (countdown <= 0)
        {
            timer.Stop();
            finished = true;
        }
    }

    /*private void OnEnable()
    {
        ResumeButton.onClick.AddListener(Pause);
    }*/

    void Update () 
	{
        UpdateTimer();
        if (Input.GetKeyDown(KeyCode.P) )
            Pause();
        if (finished)
            RemoveCountdown();
    }

    private void RemoveCountdown()
    {
        countdownScreen.enabled = false;
        Time.timeScale = 1;
        finished = false;
    }

    void UpdateTimer()
    {
        CountdownText.text = countdown + "";
    }

    public void Pause()
    {
        pause = !pause;
        Time.timeScale = Convert.ToInt32(pause);
        if (pause)
            timer.Start();
        else
            timer.Stop();
        if (countdownScreen.enabled)
            Time.timeScale = 0;

        pauseScreen.enabled = !pause;
    }
}
