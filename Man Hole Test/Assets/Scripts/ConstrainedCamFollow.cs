using System;
using UnityEngine;

public class ConstrainedCamFollow : MonoBehaviour
{
    private Camera mainCam;

    public Transform target;
    public Transform playerDeath;
    // Use this for initialization
    private void Start()
    {
        transform.parent = null;
        mainCam = Camera.main;
    }


    // Update is called once per frame
    private void Update()
    {
        if (playerDeath != null)
        {
            Vector3 newPos = new Vector3(0, target.position.y, -10);
            transform.position = newPos;
        }
        else
        {
            mainCam.cullingMask = 1 << 9;
        }
    }
}
