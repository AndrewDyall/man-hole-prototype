﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Movement25D : MonoBehaviour {

    public float speed;
    public float groundCheckDistance = 0.3f;
    public float jumpPower;
    public LayerMask groundLayers;
    public int jumpDelay;
    public int jumpMaxDuration;
    public int torchPower;
    public int batteryRestore;
    
    private Rigidbody rb;
    private Animator animMachine;
    private bool isGrounded;
    private Vector3 groundNormal;
    private bool jumpPressed;
    private bool jumpHeld;
    private bool jumpReleased;
    private bool keepJumpReleased;
    private bool moveLeft;
    private bool moveRight;
    private Stopwatch jumpDuration = new Stopwatch();
    private Stopwatch jumpInputBuffer = new Stopwatch();

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        animMachine = GetComponent<Animator>();
    }

    void Update()
    {
        jumpPressed = Input.GetButtonDown("Jump");
        jumpHeld = Input.GetButton("Jump");
        jumpReleased = Input.GetButtonUp("Jump");
        moveLeft = Input.GetButton("Left");
        moveRight = Input.GetButton("Right");
    }

    void FixedUpdate()
    {
        if ((!moveLeft && !moveRight && rb.velocity.x != 0) || (moveLeft && rb.velocity.x > 0) || (moveRight && rb.velocity.x < 0))
        {
            rb.velocity = new Vector3(0, rb.velocity.y, rb.velocity.z);
        }
        else
        {
            if (moveRight && transform.eulerAngles.y != 90)
            {
                transform.eulerAngles = new Vector3(0, 90, 0);
            }
            else if (moveLeft && transform.eulerAngles.y != -90)
            {
                transform.eulerAngles = new Vector3(0, -90, 0);
            }
        }
        CheckGroundStatus();
        float moveHorizontal = 0;
        if (moveLeft)
        {
            moveHorizontal = -1;
        }
        else if (moveRight)
        {
            moveHorizontal = 1;
        }
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, 0);
        rb.AddForce(movement * speed);
        if (jumpReleased)
        {
            keepJumpReleased = true;
        }
        if ((isGrounded) && (keepJumpReleased))
        {
            keepJumpReleased = !keepJumpReleased;
        }
        if ((jumpPressed) || ((jumpInputBuffer.IsRunning) && (jumpInputBuffer.ElapsedMilliseconds <= jumpDelay)))
        {
            if ((!jumpInputBuffer.IsRunning) && (!isGrounded))
            {
                jumpInputBuffer.Start();
            }
            if (isGrounded)
            {
                rb.velocity = new Vector3(rb.velocity.x, jumpPower, rb.velocity.z);
                if (!jumpDuration.IsRunning)
                {
                    jumpDuration.Start();
                }
                jumpPressed = !jumpPressed;
            }
            else if ((!isGrounded) && (jumpInputBuffer.ElapsedMilliseconds > jumpDelay))
            {
                jumpPressed = !jumpPressed;
                jumpInputBuffer.Stop();
                jumpInputBuffer.Reset();
            }
        }
        if ((jumpHeld) && (!keepJumpReleased))
        {
            if (!isGrounded)
            {
                if (!jumpDuration.IsRunning)
                {
                    jumpDuration.Start();
                }
                if (jumpDuration.ElapsedMilliseconds <= jumpMaxDuration)
                {
                    rb.velocity = new Vector3(rb.velocity.x, jumpPower, rb.velocity.z);
                }
            }
        }
        else if (rb.velocity.y > 0)
        {
            rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y / 10, rb.velocity.z);
        }
        animMachine.SetFloat("Forward", rb.velocity.x);
        animMachine.SetBool("OnGround", isGrounded);
        animMachine.SetFloat("Up", rb.velocity.y);
        if ((isGrounded) && (jumpDuration.IsRunning))
        {
            jumpDuration.Stop();
            jumpDuration.Reset();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Battery"))
        {
            //torchPower += batteryRestore
            other.gameObject.SetActive(false);
        }
    }

    void CheckGroundStatus()
    {
        RaycastHit hitInfo;
#if UNITY_EDITOR
        // helper to visualise the ground check ray in the scene view
        UnityEngine.Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * groundCheckDistance));
#endif
        // 0.1f is a small offset to start the ray from inside the character
        // it is also good to note that the transform position in the sample assets is at the base of the character
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, groundCheckDistance, groundLayers))
        {
            groundNormal = hitInfo.normal;
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
            groundNormal = Vector3.up;
        }
    }
}
